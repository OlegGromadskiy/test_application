import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:runloop_test_application/res/app_routes.dart';
import 'package:runloop_test_application/store/navigator_state/navigation_bloc.dart';
import 'package:runloop_test_application/store/navigator_state/navigation_state.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext _) {
    return BlocProvider<NavigationBloc>(
      create: (context) => NavigationBloc(),
      child: Builder(
        builder: (context) {
          return MaterialApp(
            navigatorKey: NavigationState.navigatorKey,
            onGenerateRoute: context.read<NavigationBloc>().state.onGenerateRoute,
            debugShowCheckedModeBanner: false,
            initialRoute: AppRoutes.home,
          );
        },
      ),
    );
  }
}
