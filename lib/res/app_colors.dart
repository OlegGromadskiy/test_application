import 'package:flutter/cupertino.dart';

abstract class AppColors{
  static const Color nebula = const Color(0xFFD0DED8);
  static const Color cuttySark = const Color(0xFF588176);
  static const Color como = const Color(0xFF588B76);
  static const Color black = const Color(0xFF000000);
  static const Color everglade = const Color(0xFF18392B);
  static const Color white = const Color(0xFFFFFFFF);


  static const Color transparentLight = const Color(0x00FFFFFF);
  static const Color transparentDark = const Color(0x00000000);
}