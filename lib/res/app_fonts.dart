import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:runloop_test_application/res/app_colors.dart';

abstract class AppFonts{
  static TextStyle robotoLight = GoogleFonts.roboto(
    fontWeight: FontWeight.w300,
    color: AppColors.nebula,
  );
  static TextStyle robotoNormal = GoogleFonts.roboto(
    fontWeight: FontWeight.w400,
    color: AppColors.nebula,
  );
  static TextStyle robotoMedium = GoogleFonts.roboto(
    fontWeight: FontWeight.w500,
    color: AppColors.nebula,
  );
  static TextStyle robotoBold = GoogleFonts.roboto(
    fontWeight: FontWeight.w700,
    color: AppColors.nebula,
  );
}