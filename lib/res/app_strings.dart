abstract class AppStrings {

  static const String emptyString = '';
  static const String space = ' ';

  static const String restApiBaseURL = 'https://randomuser.me/api/';
}
