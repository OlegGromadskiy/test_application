import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:runloop_test_application/network/models/rest_api_response_model/rest_api_response_model.dart';
import 'package:runloop_test_application/network/rest_api/rest_client.dart';

@injectable
class PeopleRepository{
  final RestClient _restClient = RestClient(Dio());

  Future<RestAPIResponseModel>? getPeople({required int count}) async {
    return await _restClient.getPeople(count.toString());
  }
}