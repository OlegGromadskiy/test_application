import 'package:json_annotation/json_annotation.dart';

part 'dob.g.dart';

@JsonSerializable()
class Dob {
  final String? date;
  final int? age;

  Dob({this.date, this.age});

  factory Dob.fromJson(Map<String, dynamic> json) => _$DobFromJson(json);
  Map<String, dynamic> toJson() => _$DobToJson(this);
}