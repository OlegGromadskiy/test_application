import 'package:runloop_test_application/entities/enums/gender.dart';
import 'package:runloop_test_application/network/models/dob/dob.dart';
import 'package:runloop_test_application/network/models/id/id.dart';
import 'package:runloop_test_application/network/models/location/location.dart';
import 'package:runloop_test_application/network/models/login/login.dart';
import 'package:runloop_test_application/network/models/name/name.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:runloop_test_application/network/models/picture/picture.dart';
import 'package:runloop_test_application/network/models/registered/registered.dart';

part 'person.g.dart';

@JsonSerializable()
class Person {
  final Gender? gender;
  final Name? name;
  final Location? location;
  final String? email;
  final Login? login;
  final Dob? dob;
  final Registered? registered;
  final String? phone;
  final String? cell;
  final Id? id;
  final Picture? picture;
  final String? nat;

  Person({
    this.gender,
    this.name,
    this.location,
    this.email,
    this.login,
    this.dob,
    this.registered,
    this.phone,
    this.cell,
    this.id,
    this.picture,
    this.nat,
  });

  factory Person.fromJson(Map<String, dynamic> json) => _$PersonFromJson(json);
  Map<String, dynamic> toJson() => _$PersonToJson(this);
}



