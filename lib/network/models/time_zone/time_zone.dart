import 'package:json_annotation/json_annotation.dart';

part 'time_zone.g.dart';

@JsonSerializable()
class TimeZone {
  final String? offset;
  final String? description;

  TimeZone({this.offset, this.description});

  factory TimeZone.fromJson(Map<String, dynamic> json) => _$TimeZoneFromJson(json);
  Map<String, dynamic> toJson() => _$TimeZoneToJson(this);
}