import 'package:runloop_test_application/network/models/coordinates/coordinates.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:runloop_test_application/network/models/street/street.dart';
import 'package:runloop_test_application/network/models/time_zone/time_zone.dart';

part 'location.g.dart';

@JsonSerializable()
class Location {
  final Street? street;
  final String? city;
  final String? state;
  final String? country;
  @JsonKey(fromJson: postCodeToJson)
  final String? postcode;
  final Coordinates? coordinates;
  final TimeZone? timezone;

  Location({
    this.street,
    this.city,
    this.state,
    this.country,
    this.postcode,
    this.coordinates,
    this.timezone,
  });

  static String postCodeToJson(dynamic value) {
    return value.toString();
  }

  factory Location.fromJson(Map<String, dynamic> json) => _$LocationFromJson(json);

  Map<String, dynamic> toJson() => _$LocationToJson(this);
}
