import 'package:json_annotation/json_annotation.dart';
import 'package:runloop_test_application/res/app_strings.dart';

part 'name.g.dart';

@JsonSerializable()
class Name {
  final String? title;
  final String? first;
  final String? last;

  Name({this.title, this.first, this.last});

  String toSolidName(){
    return title! + AppStrings.space + first! + AppStrings.space + last!;
  }

  factory Name.fromJson(Map<String, dynamic> json) => _$NameFromJson(json);
  Map<String, dynamic> toJson() => _$NameToJson(this);
}