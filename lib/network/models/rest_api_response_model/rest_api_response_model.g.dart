// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rest_api_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RestAPIResponseModel _$RestAPIResponseModelFromJson(Map<String, dynamic> json) {
  return RestAPIResponseModel(
    (json['results'] as List<dynamic>)
        .map((e) => Person.fromJson(e as Map<String, dynamic>))
        .toList(),
    Info.fromJson(json['info'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$RestAPIResponseModelToJson(
        RestAPIResponseModel instance) =>
    <String, dynamic>{
      'results': instance.results,
      'info': instance.info,
    };
