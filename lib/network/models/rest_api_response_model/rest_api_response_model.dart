import 'package:json_annotation/json_annotation.dart';
import 'package:runloop_test_application/network/models/info/info.dart';
import 'package:runloop_test_application/network/models/person/person.dart';

part 'rest_api_response_model.g.dart';

@JsonSerializable()
class RestAPIResponseModel{
  final List<Person> results;
  final Info info;

  RestAPIResponseModel(this.results, this.info);

  factory RestAPIResponseModel.fromJson(Map<String, dynamic> json) => _$RestAPIResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$RestAPIResponseModelToJson(this);
}