import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:runloop_test_application/network/models/rest_api_response_model/rest_api_response_model.dart';
import 'package:runloop_test_application/res/app_strings.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: AppStrings.restApiBaseURL)
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET('')
  Future<RestAPIResponseModel> getPeople(@Query('results') String count);
}