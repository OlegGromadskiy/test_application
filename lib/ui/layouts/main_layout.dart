import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  final Widget? child;
  final PreferredSizeWidget? appbar;
  final Widget? bottomBar;
  final Color? backgroundColor;
  final EdgeInsets padding;

  const MainLayout({
    Key? key,
    this.child,
    this.appbar,
    this.backgroundColor,
    this.bottomBar,
    this.padding = const EdgeInsets.symmetric(horizontal: 8.0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: backgroundColor,
      appBar: appbar,
      body: child,
      bottomNavigationBar: bottomBar,
    );
  }
}
