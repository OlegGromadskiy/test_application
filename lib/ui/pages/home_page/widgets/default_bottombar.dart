import 'package:flutter/material.dart';
import 'package:runloop_test_application/res/app_colors.dart';
import 'package:runloop_test_application/res/app_fonts.dart';
import 'package:runloop_test_application/store/people_state/events/clear_people_event.dart';
import 'package:runloop_test_application/store/people_state/events/receive_people_event.dart';
import 'package:runloop_test_application/store/people_state/people_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:runloop_test_application/store/people_state/people_state.dart';

class DefaultBottomBar extends StatelessWidget {
  final double height;
  final Color? backgroundColor;
  final EdgeInsets padding;

  const DefaultBottomBar({
    Key? key,
    this.height = 100.0,
    this.backgroundColor,
    this.padding = const EdgeInsets.all(8.0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: padding,
      decoration: BoxDecoration(
        color: backgroundColor ?? AppColors.como,
        boxShadow: [
          BoxShadow(
            color: AppColors.black.withOpacity(0.7),
            offset: Offset(0, 5),
            spreadRadius: 5.0,
            blurRadius: 7.0,
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(25.0),
            child: SizedBox(
              height: 50.0,
              width: 150.0,
              child: Material(
                color: AppColors.everglade,
                child: BlocBuilder<PeopleBloc, PeopleState>(
                  builder: (ctx, state) => InkWell(
                    highlightColor: AppColors.transparentLight,
                    onTap: () => state.people.isEmpty
                        ? context.read<PeopleBloc>().add(ReceivePeopleEvent(20))
                        : context.read<PeopleBloc>().add(ClearPeopleEvent()),
                    child: Center(
                      child: state.people.isEmpty
                          ? Icon(
                              Icons.replay,
                              color: AppColors.nebula,
                            )
                          : Icon(
                              Icons.delete_forever_outlined,
                              color: AppColors.nebula,
                            ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          BlocBuilder<PeopleBloc, PeopleState>(
            builder: (ctx, state) => Text(
              state.people.isEmpty
                  ? 'Click the button above to start receiving data'
                  : 'Click the button above to clear the list and stop receiving data',
              style: AppFonts.robotoMedium.copyWith(fontSize: 14.0),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
