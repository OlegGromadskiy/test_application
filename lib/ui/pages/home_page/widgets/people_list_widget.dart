import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:runloop_test_application/res/app_colors.dart';
import 'package:runloop_test_application/store/people_state/events/pagination_event.dart';
import 'package:runloop_test_application/store/people_state/events/receive_people_event.dart';
import 'package:runloop_test_application/store/people_state/people_bloc.dart';
import 'package:runloop_test_application/store/people_state/people_state.dart';
import 'package:runloop_test_application/ui/pages/home_page/widgets/person_list_view_item.dart';
import 'package:runloop_test_application/ui/shared/fade_slide_animation.dart';

class PeopleListWidget extends StatefulWidget {
  final PeopleState state;

  const PeopleListWidget({Key? key, required this.state}) : super(key: key);

  @override
  _PeopleListWidgetState createState() => _PeopleListWidgetState();
}

class _PeopleListWidgetState extends State<PeopleListWidget> {
  late final ScrollController? _scrollController;
  late final void Function() _scrollControllerListener;

  @override
  void initState() {
    super.initState();

    _scrollController = ScrollController();
    _scrollController!.addListener(
      _scrollControllerListener = () {
        if (_scrollController!.positions.isNotEmpty && !context.read<PeopleBloc>().state.isDataLoading) {
          if (_scrollController!.offset >= _scrollController!.position.maxScrollExtent) {
            print('Receive data');
            context.read<PeopleBloc>().add(PaginationEvent(true));
            context.read<PeopleBloc>().add(ReceivePeopleEvent(20));
          }
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController!.removeListener(_scrollControllerListener);
    _scrollController!.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: CustomScrollView(
        controller: _scrollController,
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(child: SizedBox(height: 88.0)),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (_, index) => FadeSlideAnimation(
                child: PersonListViewItem(person: widget.state.people[index]),
              ),
              childCount: widget.state.people.length,
            ),
          ),
          if (widget.state.isDataLoading)
            SliverFillRemaining(
              hasScrollBody: false,
              fillOverscroll: true,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Center(
                  child: CircularProgressIndicator(
                    color: AppColors.como,
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
