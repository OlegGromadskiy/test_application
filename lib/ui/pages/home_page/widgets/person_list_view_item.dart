import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:runloop_test_application/entities/enums/gender.dart';
import 'package:runloop_test_application/network/models/person/person.dart';
import 'package:runloop_test_application/res/app_colors.dart';
import 'package:runloop_test_application/res/app_fonts.dart';
import 'package:runloop_test_application/res/app_images.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:runloop_test_application/res/app_routes.dart';
import 'package:runloop_test_application/store/events/push_event.dart';
import 'package:runloop_test_application/store/navigator_state/navigation_bloc.dart';
import 'package:runloop_test_application/ui/pages/user_details_page/user_details_page_data.dart';

class PersonListViewItem extends StatelessWidget {
  final Person person;

  const PersonListViewItem({Key? key, required this.person}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Material(
          borderRadius: BorderRadius.circular(15),
          color: AppColors.cuttySark,
          child: InkWell(
            highlightColor: AppColors.transparentLight,
            onTap: () => context.read<NavigationBloc>().add(
                  PushEvent(
                    routeName: AppRoutes.userDetails,
                    pageData: UserDetailsPageData(person),
                  ),
                ),
            splashColor: AppColors.everglade.withOpacity(0.3),
            child: Container(
              height: 80.0,
              padding: const EdgeInsets.only(
                top: 4.0,
                bottom: 4.0,
                left: 4.0,
                right: 8.0,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: CachedNetworkImage(
                      imageUrl: person.picture?.large ?? '',
                      imageBuilder: (context, imageProvider) => Image(image: imageProvider),
                      errorWidget: (context, url, error) {
                        return Image.asset(
                          person.gender == Gender.female ? AppImages.female_placeholder : AppImages.male_placeholder,
                        );
                      },
                      placeholder: (context, url) {
                        return Image.asset(
                          person.gender == Gender.female ? AppImages.female_placeholder : AppImages.male_placeholder,
                        );
                      },
                    ),
                  ),
                  SizedBox(width: 4.0),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          person.name?.toSolidName() ?? '',
                          style: AppFonts.robotoMedium.copyWith(fontSize: 16.0),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          person.dob?.age != null ? '${person.dob!.age} years old' : '',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: AppFonts.robotoMedium.copyWith(fontSize: 12.0),
                        ),
                        Spacer(),
                        Text(
                          person.email ?? '',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: AppFonts.robotoLight.copyWith(fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 20.0),
                  Text(
                    person.registered?.age != null ? '${person.registered!.age} years\nwith us ' : '',
                    textAlign: TextAlign.center,
                    style: AppFonts.robotoNormal.copyWith(fontSize: 12.0),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
