import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:runloop_test_application/res/app_colors.dart';
import 'package:runloop_test_application/res/app_fonts.dart';
import 'package:runloop_test_application/res/app_strings.dart';

class DefaultAppbar extends StatelessWidget implements PreferredSizeWidget {
  final double height;
  final String title;
  final Widget? subTitle;
  final Color? backgroundColor;
  final EdgeInsets padding;

  DefaultAppbar({
    Key? key,
    this.height = 80.0,
    this.title = AppStrings.emptyString,
    this.subTitle,
    this.backgroundColor,
    this.padding = const EdgeInsets.all(12.0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: preferredSize.height,
      padding: padding,
      decoration: BoxDecoration(
        color: backgroundColor ?? AppColors.como,
        borderRadius: BorderRadius.vertical(bottom: Radius.circular(25.0)),
        boxShadow: [
          BoxShadow(
            color: AppColors.black.withOpacity(0.7),
            offset: Offset(0, -5),
            spreadRadius: 3.0,
            blurRadius: 7.0,
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(height: MediaQuery.of(context).viewPadding.top),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                title,
                style: AppFonts.robotoBold.copyWith(fontSize: 20.0),
              ),
              subTitle ?? SizedBox(),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
