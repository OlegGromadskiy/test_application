import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:runloop_test_application/res/app_colors.dart';
import 'package:runloop_test_application/res/app_fonts.dart';
import 'package:runloop_test_application/store/people_state/events/pagination_event.dart';
import 'package:runloop_test_application/store/people_state/events/receive_people_event.dart';
import 'package:runloop_test_application/store/people_state/people_bloc.dart';
import 'package:runloop_test_application/store/people_state/people_state.dart';
import 'package:runloop_test_application/ui/layouts/main_layout.dart';
import 'package:runloop_test_application/ui/pages/home_page/widgets/default_appbar.dart';
import 'package:runloop_test_application/ui/pages/home_page/widgets/default_bottombar.dart';
import 'package:runloop_test_application/ui/pages/home_page/widgets/people_list_widget.dart';
import 'package:runloop_test_application/ui/pages/home_page/widgets/person_list_view_item.dart';
import 'package:runloop_test_application/ui/shared/fade_slide_animation.dart';
import 'package:runloop_test_application/utils/bloc_builder_with_post_frame_callback.dart';
import 'package:runloop_test_application/utils/custom_stateful_builder.dart';

class HomePage extends StatelessWidget {


  @override
  Widget build(BuildContext _) {
    return BlocProvider<PeopleBloc>(
      create: (_) => PeopleBloc(),
      child: MainLayout(
        appbar: DefaultAppbar(
          title: 'Home page',
          subTitle: BlocBuilder<PeopleBloc, PeopleState>(
            builder: (ctx, state) => Text(
              '${state.people.length} users',
              style: AppFonts.robotoBold.copyWith(fontSize: 14.0),
            ),
          ),
        ),
        child: BlocBuilderWithPostFrameCallback<PeopleBloc, PeopleState>(
          postFrameCallBack: (bloc) => bloc.add(ReceivePeopleEvent(20)),
          builder: (ctx, state) {
            if (state.people.isEmpty) {
              return Center(
                child: CircularProgressIndicator(
                  color: AppColors.como,
                ),
              );
            }
            return PeopleListWidget(state: state);
          },
        ),
        bottomBar: DefaultBottomBar(),
      ),
    );
  }
}
