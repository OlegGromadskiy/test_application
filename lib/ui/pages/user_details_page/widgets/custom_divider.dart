import 'package:flutter/material.dart';
import 'package:runloop_test_application/res/app_colors.dart';

class CustomDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: ColoredBox(
        color: AppColors.como,
        child: SizedBox(
          height: 4.0,
          width: double.infinity,
        ),
      ),
    );
  }
}
