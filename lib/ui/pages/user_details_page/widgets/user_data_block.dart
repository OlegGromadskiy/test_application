import 'package:flutter/material.dart';
import 'package:runloop_test_application/res/app_colors.dart';
import 'package:runloop_test_application/res/app_fonts.dart';

class UsersDataBlock extends StatelessWidget {
  final List<String> points;
  final String paragraph;
  final IconData icon;
  final EdgeInsets iconPadding;

  const UsersDataBlock({
    Key? key,
    required this.points,
    required this.paragraph,
    required this.icon, this.iconPadding = const EdgeInsets.all(0.0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          children: [
            Padding(
              padding: iconPadding,
              child: Icon(icon),
            ),
            Text(
              paragraph,
              style: AppFonts.robotoMedium.copyWith(
                color: AppColors.everglade,
                fontSize: 22.0,
              ),
            ),
          ],
        ),
        const SizedBox(height: 5.0),
        ...points.map(
          (element) => Text(
            element,
            style: AppFonts.robotoLight.copyWith(
              color: AppColors.everglade,
              fontSize: 18.0,
            ),
          ),
        ),
      ],
    );
  }
}
