import 'package:runloop_test_application/entities/classes/ipage_data.dart';
import 'package:runloop_test_application/network/models/person/person.dart';

class UserDetailsPageData extends IPageData{
  final Person person;

  UserDetailsPageData(this.person);
}