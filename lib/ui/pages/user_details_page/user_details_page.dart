import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:runloop_test_application/entities/enums/gender.dart';
import 'package:runloop_test_application/network/models/person/person.dart';
import 'package:runloop_test_application/res/app_colors.dart';
import 'package:runloop_test_application/res/app_fonts.dart';
import 'package:runloop_test_application/res/app_images.dart';
import 'package:runloop_test_application/store/events/pop_event.dart';
import 'package:runloop_test_application/store/navigator_state/navigation_bloc.dart';
import 'package:runloop_test_application/ui/layouts/main_layout.dart';
import 'package:runloop_test_application/ui/pages/user_details_page/widgets/custom_divider.dart';
import 'package:runloop_test_application/ui/pages/user_details_page/widgets/user_data_block.dart';
import 'package:runloop_test_application/utils/custom_scroll_behavior.dart';
import 'package:runloop_test_application/utils/date_extension.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserDetailsPage extends StatelessWidget {
  final Person person;

  const UserDetailsPage({Key? key, required this.person}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      backgroundColor: AppColors.cuttySark,
      child: ScrollConfiguration(
        behavior: CustomScrollBehavior(),
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              expandedHeight: 300.0,
              backgroundColor: AppColors.cuttySark,
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(360.0),
                child: Material(
                  color: AppColors.transparentLight,
                  child: InkWell(
                    splashColor: AppColors.como,
                    highlightColor: AppColors.transparentLight,
                    onTap: () => context.read<NavigationBloc>().add(PopEvent()),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: AppColors.white,
                      ),
                    ),
                  ),
                ),
              ),
              flexibleSpace: SizedBox(
                height: 350.0,
                child: Stack(
                  children: [
                    Positioned.fill(
                      child: CachedNetworkImage(
                        imageUrl: person.picture?.large ?? '',
                        imageBuilder: (context, imageProvider) => Image(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                        placeholder: (context, url) {
                          return Image.asset(person.gender == Gender.male ? AppImages.male_placeholder : AppImages.female_placeholder);
                        },
                        errorWidget: (context, url, error) {
                          return Image.asset(person.gender == Gender.male ? AppImages.male_placeholder : AppImages.female_placeholder);
                        },
                      ),
                    ),
                    Positioned(
                      bottom: 0.0,
                      right: 0.0,
                      left: 0.0,
                      child: Transform(
                        transform: Matrix4.translationValues(0.0, 1.0, 0.0),
                        child: Container(
                          height: 20.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0)),
                            color: AppColors.nebula,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: ColoredBox(
                color: AppColors.nebula,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 30.0),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                          person.name?.toSolidName() ?? '',
                          style: AppFonts.robotoBold.copyWith(
                            color: AppColors.everglade,
                            fontSize: 26.0,
                          ),
                        ),
                      ),
                      CustomDivider(),
                      UsersDataBlock(
                        points: [
                          'Age: ${person.dob?.age ?? '-'} years old',
                          'Birth date: ${person.dob?.date == null ? '-' : DateTime.parse(person.dob!.date!).ddMMMMyyyy}',
                          'Gender: ${person.gender.toString().split('.').last}',
                        ],
                        paragraph: 'Personal:',
                        icon: Icons.person_search,
                      ),
                      CustomDivider(),
                      UsersDataBlock(
                        points: [
                          'Country: ${person.location?.country ?? '-'}',
                          'State: ${person.location?.state ?? '-'}',
                          'City: ${person.location?.city ?? '-'}',
                          'Street: ${person.location?.street?.name ?? '-'}',
                          'Street number: ${person.location?.street?.number?.toString() ?? '-'}',
                        ],
                        paragraph: 'Location:',
                        icon: Icons.location_on,
                      ),
                      CustomDivider(),
                      UsersDataBlock(
                        points: [
                          'Email: ${person.email ?? '-'}',
                          'Phone: ${person.phone ?? '-'}',
                          'Cell: ${person.phone ?? '-'}',
                        ],
                        paragraph: 'Contacts:',
                        icon: Icons.contact_mail,
                        iconPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                      ),
                      CustomDivider(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _comoSizedBox() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const SizedBox(height: 16.0),
        ColoredBox(
          color: AppColors.como,
          child: SizedBox(
            height: 4.0,
          ),
        ),
        const SizedBox(height: 16.0),
      ],
    );
  }
}
