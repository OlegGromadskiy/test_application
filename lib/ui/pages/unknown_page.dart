import 'package:flutter/material.dart';
import 'package:runloop_test_application/res/app_colors.dart';
import 'package:runloop_test_application/res/app_fonts.dart';

class UnknownPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.white,
      child: Center(
        child: Text(
          'UnknownPage',
          style: AppFonts.robotoBold.copyWith(fontSize: 48.0),
        ),
      ),
    );
  }
}
