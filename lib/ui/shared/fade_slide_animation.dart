import 'package:flutter/material.dart';

class FadeSlideAnimation extends StatefulWidget {
  final Widget child;

  const FadeSlideAnimation({Key? key, required this.child}) : super(key: key);

  @override
  _FadeSlideAnimationState createState() => _FadeSlideAnimationState();
}

class _FadeSlideAnimationState extends State<FadeSlideAnimation>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 700),
      value: 0.0,
    )..forward();
  }

  @override
  void dispose() {
    _animationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return FadeTransition(
          opacity: Tween<double>(begin: 0.0, end: 1.0).animate(_animationController),
          child: SlideTransition(
            position: Tween<Offset>(begin: Offset(10.0, 0.0), end: Offset.zero).animate(_animationController),
            child: widget.child,
          ),
        );
      },
    );
  }
}
