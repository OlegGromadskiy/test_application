import 'package:flutter/material.dart';
import 'package:runloop_test_application/entities/classes/ipage_data.dart';
import 'package:runloop_test_application/res/app_routes.dart';
import 'package:runloop_test_application/ui/pages/home_page/home_page.dart';
import 'package:runloop_test_application/ui/pages/unknown_page.dart';
import 'package:runloop_test_application/ui/pages/user_details_page/user_details_page.dart';
import 'package:runloop_test_application/ui/pages/user_details_page/user_details_page_data.dart';

class NavigationState {
  final List<String> routes;

  String? get currentRoute => routes.isNotEmpty ? routes.last : null;
  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  NavigationState({required this.routes});

  NavigationState copyWith({
    List<String>? routes,
  }) {
    return NavigationState(
      routes: routes ?? this.routes,
    );
  }

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    routes.add(settings.name!);

    switch (settings.name) {
      case AppRoutes.home:
        return MaterialPageRoute(builder: (context) => HomePage());
      case AppRoutes.userDetails:
        return MaterialPageRoute(builder: (context) => UserDetailsPage(person: (settings.arguments as UserDetailsPageData).person));
      default:
        return MaterialPageRoute(builder: (context) => UnknownPage());
    }
  }

  void pop(){
    routes.removeLast();
    Navigator.of(navigatorKey.currentContext!).pop();
  }

  void push({required String routeName, IPageData? pageData}){
    routes.add(routeName);
    Navigator.of(navigatorKey.currentContext!).pushNamed(routeName, arguments: pageData);
  }
}
