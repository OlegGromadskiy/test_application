import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:runloop_test_application/entities/classes/ievent.dart';
import 'package:runloop_test_application/store/events/pop_event.dart';
import 'package:runloop_test_application/store/events/push_event.dart';
import 'package:runloop_test_application/store/navigator_state/navigation_state.dart';

class NavigationBloc extends Bloc<IEvent, NavigationState> {
  NavigationBloc() : super(NavigationState(routes: []));

  @override
  Stream<NavigationState> mapEventToState(IEvent event) async* {
    switch (event.runtimeType) {
      case PushEvent:
        yield _push(event as PushEvent);
        break;
      case PopEvent:
        yield _pop(event as PopEvent);
        break;
    }
  }

  NavigationState _pop(PopEvent event) {
    state.pop();
    return state.copyWith();
  }

  NavigationState _push(PushEvent event) {
    state.push(routeName: event.routeName, pageData: event.pageData);
    return state.copyWith();
  }
}
