import 'package:runloop_test_application/network/models/person/person.dart';

class PeopleState {
  final List<Person> people;
  final bool isDataLoading;

  const PeopleState({
    this.isDataLoading = false,
    required this.people,
  });

  PeopleState copyWith({
    List<Person>? people,
    bool? isDataLoading,
  }) {
    return PeopleState(
      people: people ?? this.people,
      isDataLoading: isDataLoading ?? this.isDataLoading,
    );
  }
}
