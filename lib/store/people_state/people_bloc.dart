import 'package:bloc/bloc.dart';
import 'package:runloop_test_application/entities/classes/ievent.dart';
import 'package:runloop_test_application/network/models/person/person.dart';
import 'package:runloop_test_application/network/repositories/people_repository.dart';
import 'package:runloop_test_application/store/people_state/events/clear_people_event.dart';
import 'package:runloop_test_application/store/people_state/events/pagination_event.dart';
import 'package:runloop_test_application/store/people_state/events/receive_people_event.dart';
import 'package:runloop_test_application/store/people_state/people_state.dart';
import 'package:runloop_test_application/utils/get_it.dart';

class PeopleBloc extends Bloc<IEvent, PeopleState> {
  PeopleBloc() : super(PeopleState(people: []));

  @override
  Stream<PeopleState> mapEventToState(IEvent event) async* {
    switch (event.runtimeType) {
      case ReceivePeopleEvent:
        yield* _receivePeopleEvent(event as ReceivePeopleEvent);
        break;
      case ClearPeopleEvent:
        yield* _clearPeopleEvent(event as ClearPeopleEvent);
        break;
      case PaginationEvent:
        yield* _paginationEvent(event as PaginationEvent);
        break;
    }
  }

  Stream<PeopleState> _receivePeopleEvent(ReceivePeopleEvent receivePeopleEvent) async* {
    yield state.copyWith(isDataLoading: true);

    final List<Person> persons =
        (await getIt.get<PeopleRepository>().getPeople(count: receivePeopleEvent.count))!.results;

    yield state.copyWith(people: state.people..addAll(persons));

    await Future.delayed(Duration(milliseconds: 333));

    yield state.copyWith(isDataLoading: false);
  }

  Stream<PeopleState> _clearPeopleEvent(ClearPeopleEvent _) {
    return Stream.value(PeopleState(people: []));
  }

  Stream<PeopleState> _paginationEvent(PaginationEvent paginationEvent) {
    return Stream.value(state.copyWith(isDataLoading: paginationEvent.isDataLoading));
  }
}
