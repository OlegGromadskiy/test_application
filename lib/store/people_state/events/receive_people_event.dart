import 'package:runloop_test_application/entities/classes/ievent.dart';

class ReceivePeopleEvent extends IEvent{
  final int count;

  ReceivePeopleEvent(this.count);
}