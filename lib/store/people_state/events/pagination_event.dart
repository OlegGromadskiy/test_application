import 'package:runloop_test_application/entities/classes/ievent.dart';

class PaginationEvent extends IEvent{
  final bool isDataLoading;

  PaginationEvent(this.isDataLoading);
}