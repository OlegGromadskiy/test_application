import 'package:runloop_test_application/entities/classes/ievent.dart';
import 'package:runloop_test_application/entities/classes/ipage_data.dart';

class PushEvent extends IEvent{
  final String routeName;
  final IPageData? pageData;

  PushEvent({required this.routeName, this.pageData});
}