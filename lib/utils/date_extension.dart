import 'package:intl/intl.dart';

extension DateFormats on DateTime{
  String get ddMMMMyyyy => DateFormat('dd MMMM yyyy').format(this);
}

