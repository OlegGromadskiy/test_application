import 'package:flutter/material.dart';

class CustomStatefulBuilder extends StatefulWidget {
  final void Function(BuildContext context, void Function(void Function()) setState)? initState;
  final void Function()? dispose;
  final Widget Function(BuildContext context, void Function(void Function()) setState) builder;

  const CustomStatefulBuilder({
    Key? key,
    this.initState,
    required this.builder, this.dispose,
  }) : super(key: key);

  @override
  _CustomStatefulBuilderState createState() => _CustomStatefulBuilderState();
}

class _CustomStatefulBuilderState extends State<CustomStatefulBuilder> {
  @override
  void initState() {
    super.initState();

    widget.initState?.call(context, setState);
  }

  @override
  void dispose() {
    widget.dispose?.call();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder.call(context, setState);
  }
}
