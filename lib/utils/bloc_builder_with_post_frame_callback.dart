import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocBuilderWithPostFrameCallback<B extends BlocBase<S>, S> extends StatefulWidget {
  final BlocWidgetBuilder<S> builder;
  final BlocBuilderCondition<S>? buildWhen;
  final void Function(B bloc)? postFrameCallBack;
  final B? bloc;

  const BlocBuilderWithPostFrameCallback({
    required this.builder,
    this.buildWhen,
    this.bloc,
    this.postFrameCallBack,
  });

  @override
  _BlocBuilderWithPostFrameCallbackState<B, S> createState() => _BlocBuilderWithPostFrameCallbackState<B, S>();
}

class _BlocBuilderWithPostFrameCallbackState<B extends BlocBase<S>, S>
    extends State<BlocBuilderWithPostFrameCallback<B, S>> {

  late B? bloc;

  @override
  void initState() {
    super.initState();

    bloc = widget.bloc ?? context.read<B>();

    if(widget.postFrameCallBack != null){
      WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
        widget.postFrameCallBack!.call(bloc!);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<B, S>(
      builder: widget.builder,
      bloc: widget.bloc,
      buildWhen: widget.buildWhen,
    );
  }
}
