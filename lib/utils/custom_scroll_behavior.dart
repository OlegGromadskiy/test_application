import 'package:flutter/material.dart';
import 'package:runloop_test_application/res/app_colors.dart';

class CustomScrollBehavior extends ScrollBehavior {
  final Color color;

  CustomScrollBehavior({this.color = AppColors.como});
  @override
  Widget buildViewportChrome(BuildContext context, Widget child, AxisDirection axisDirection) {
    return GlowingOverscrollIndicator(
      axisDirection: axisDirection,
      color: color,
      child: child,
    );
  }
}
