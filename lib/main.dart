import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:runloop_test_application/application/application.dart';
import 'package:runloop_test_application/utils/get_it.dart';

void main() async {
  await initSystem();

  runApp(Application());
}

Future<void> initSystem() async {
  WidgetsFlutterBinding.ensureInitialized();

  configureDependencies();

  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarBrightness: Brightness.light,
      statusBarColor: Colors.transparent,
    ),
  );
}
